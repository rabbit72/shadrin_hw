import os


def hardlink_check(directory_path: str) -> bool:
    file_inodes = set()
    try:
        with os.scandir(directory_path) as directory:
            for file in directory:
                inode = file.inode()
                if inode in file_inodes:
                    return True
                file_inodes.add(inode)
    except (NotADirectoryError, FileNotFoundError, PermissionError):
        """
        В случае плохого аргумента или возникновения какого-либо 
        исключения - функция должна вернуть False
        """
    return False


if __name__ == "__main__":
    dir = "."
    print(hardlink_check(dir))
