import unittest

from .shadrin_hw4 import brackets_trim


class TestBracketsTrim(unittest.TestCase):
    def test_correct_return(self):
        cases = [
            ("(a*(b/c)+((d-f)/k))", "a*b/c+(d-f)/k"),
            ("a", "a"),
            ("(a)", "a"),
            ("a*(b+c)", "a*(b+c)"),
            ("a+(b+c)", "a+b+c"),
            ("(a*(b/c)+((d-f)/k))*(h*(g-r))", "(a*b/c+(d-f)/k)*h*(g-r)"),
            ("(x+y)/(j*z)+g", "(x+y)/(j*z)+g"),
            ("(x + y) / (j * z) + g", "(x+y)/(j*z)+g"),
            ("(x * y) / (j * z) + g", "x*y/(j*z)+g"),
            ("(x*y)+(j*z)+g", "x*y+j*z+g"),
            ("(x*y)-(j*z)+g", "x*y-j*z+g"),
            ("a-(b+c)", "a-(b+c)"),
        ]
        for args, result in cases:
            with self.subTest(args=args):
                self.assertEqual(brackets_trim(args), result)
