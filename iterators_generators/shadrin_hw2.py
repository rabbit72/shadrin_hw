import os.path
from tempfile import NamedTemporaryFile


def get_next_number(file_descriptor):
    line = file_descriptor.readline().strip()
    if line:
        line = int(line)
    return line


def write_in_file(number, file_descriptor):
    file_descriptor.write("{}\n".format(number))


def merge_files(fname1: str, fname2: str) -> str:
    if not os.path.isfile(fname1) or not os.path.isfile(fname2):
        raise FileNotFoundError("Pass correct file paths")
    with NamedTemporaryFile(mode="w", delete=False) as file_result:
        result_file_name = file_result.name
        with open(fname1) as file1, open(fname2) as file2:
            less = get_next_number(file1), file1
            more = get_next_number(file2), file2
            while True:

                if more[0] == "":
                    write_in_file(less[0], file_result)
                    for number in less[1]:
                        file_result.write(number)
                    break
                elif less[0] == "":
                    write_in_file(more[0], file_result)
                    for number in more[1]:
                        file_result.write(number)
                    break

                if less[0] > more[0]:
                    less, more = more, less
                write_in_file(less[0], file_result)
                less, more = more, (get_next_number(less[1]), less[1])
    return result_file_name


if __name__ == "__main__":
    file_path1 = "file1.txt"
    file_path2 = "file2.txt"
    merged_file = merge_files(file_path1, file_path2)
    print(merged_file)
