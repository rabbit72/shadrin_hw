class Graph:
    def __init__(self, dict_with_graph):
        self.dict_with_graph = dict_with_graph

    def __getitem__(self, item):
        return self.dict_with_graph.get(item, None)


class GraphIterator:
    def __init__(self, graph, start_v):
        self.graph = graph
        self.queue = [start_v]
        self.last = set()
        self.i = 0

    def has_next(self) -> bool:
        return self.i < len(self.queue)

    def __iter__(self):
        return self

    def next(self) -> str:
        if not self.has_next():
            raise StopIteration
        current_point = self.queue[self.i]
        self.last.add(current_point)
        self.i += 1
        other_points_from_current_point = self.graph[current_point]
        for point in other_points_from_current_point:
            if point not in self.last:
                self.queue.append(point)
                self.last.add(point)
        return current_point

    __next__ = next


if __name__ == "__main__":
    graph = Graph({
        'a': ['b', 'c', 'd', 'e'],
        'b': ['a', 'c', 'f'],
        'c': ['a', 'b', 'f', 'g'],
        'd': ['a', 'e', 'g'],
        'e': ['a', 'd'],
        'f': ['b', 'c'],
        'g': ['c', 'd']
    })
    iterator = GraphIterator(graph, "a")
    print("next() - {}".format(next(iterator)))
    for i in iterator:
        print("__next__() - {}".format(i))
