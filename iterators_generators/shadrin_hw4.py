def available_operations():
    return {"+": 1, "-": 1, "*": 2, "/": 2, None: 3}


def get_postfix(infix):
    operations = available_operations()
    stack = []
    output = ""
    for symbol in infix:
        if not symbol:
            continue
        elif symbol.isalpha():
            output += symbol
        elif symbol == "(":
            stack.append(symbol)
        elif symbol in ")":
            while stack and stack[-1] != "(":
                output += stack.pop()
            stack.pop()
        elif symbol in operations:
            while stack and operations.get(stack[-1], 0) >= operations[symbol]:
                output += stack.pop()
            stack.append(symbol)
    for operation in stack[::-1]:
        output += operation
    return output


def get_infix(postfix):
    operations = available_operations()
    stack = []
    for symbol in postfix:
        if not symbol:
            continue
        elif symbol.isalpha():
            stack.append([symbol, None])
        elif symbol in operations:
            second = stack.pop()
            first = stack.pop()
            priority_current = operations[symbol]
            priority_first = operations.get(first[1])
            priority_second = operations.get(second[1])

            unusual_case = (priority_second == priority_current and
                            symbol in {"/", "-"})
            if priority_second < priority_current or unusual_case:
                second[0] = "(" + second[0] + ")"
            if priority_first < priority_current:
                first[0] = "(" + first[0] + ")"

            stack.append([first[0] + symbol + second[0], symbol])
    return stack.pop()[0]


def brackets_trim(input_data: str) -> str:
    postfix = get_postfix(input_data)
    infix = get_infix(postfix)
    return infix
