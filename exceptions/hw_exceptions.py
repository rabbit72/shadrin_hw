import os.path


class ParallelWorkingError(Exception):
    pass


class pidfile:
    def __init__(self, file_name: str):
        self.file_name = file_name

        if not os.path.isfile(self.file_name):
            with open(self.file_name, "w") as new_file:
                pass

        if self.is_lock():
            raise ParallelWorkingError("There is already working")
        self.lock()

    def __enter__(self):
        pass

    def __exit__(self, *exc_info):
        self.unlock()

    def is_lock(self) -> bool:
        with open(self.file_name) as f:
            flag = f.read()
            if flag == "1":
                return True
            elif flag == "0":
                return False
            else:
                return False

    def lock(self):
        with open(self.file_name, "w") as f:
            f.write("1")

    def unlock(self):
        with open(self.file_name, "w") as f:
            f.write("0")
