import sys


class change_stderr:
    def __init__(self, file):
        self.file = file
        if file:
            self.file = open(file, "a")
        if self.file and sys.stderr is sys.__stderr__:
            sys.stderr = self.file

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        if sys.stderr is self.file:
            sys.stderr.close()
            sys.stderr = sys.__stderr__
            self.file.close()


def stderr_redirect(dest=None):
    def wrapper_1(func):
        def wrapper_2():
            with change_stderr(dest):
                result = func()
            return result
        return wrapper_2
    return wrapper_1


@stderr_redirect(dest=None)
def test():
    sys.stderr.write('test\n')
    return 10


@stderr_redirect(dest='test1')
def test1():
    sys.stderr.write('test1 func\n')
    return 10


@stderr_redirect(dest='test2')
def test2():
    test1()
    sys.stderr.write('test2 func\n')
    test1()
    return 10


if __name__ == "__main__":
    print(test())
    print(test1())
    print(test2())
