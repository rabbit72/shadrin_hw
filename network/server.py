import json
from copy import copy
from socketserver import ThreadingTCPServer, StreamRequestHandler


def singleton(cls):
    instances = {}

    def get_instance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]

    return get_instance


@singleton
class Core:
    def __init__(self):
        self.current_users_on_server = dict()  # nickname: conn

    @staticmethod
    def _send_message(conn, name, message):
        conn.sendall(f"{name}: {message}".encode("utf8"))

    def send_message(self, message, sender, recipient=None):
        conn = self.current_users_on_server.get(recipient, None)
        if not recipient:
            for user_name, conn in copy(self.current_users_on_server).items():
                try:
                    self._send_message(conn, sender, message)
                except BrokenPipeError:
                    del self.current_users_on_server[user_name]
        elif conn:
            self._send_message(conn, f"<{sender}>", f"{message}")

    def get_all_users(self, user_name):
        conn = self.current_users_on_server.get(user_name)
        if conn:
            json_all_users = json.dumps(list(self.current_users_on_server.keys()))
            conn.sendall(json_all_users.encode("utf8"))

    def add_user(self, user_name, user_connection):
        conn = self.current_users_on_server.get(user_name, None)
        if conn:
            return False
        self.current_users_on_server[user_name] = user_connection
        return user_name

    def delete_user(self, user_name):
        if user_name not in self.current_users_on_server:
            return False
        del self.current_users_on_server[user_name]
        return True


class ChatHandler(StreamRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user_name = None

    def register_new_user(self):
        user_name = self.connection.recv(4096).decode("utf8").strip()
        is_correct_registration = Core().add_user(user_name, self.connection)
        self.user_name = user_name
        return is_correct_registration

    @staticmethod
    def prepare_private_message(data):
        raw_recipient, raw_message = data.split(":", maxsplit=1)
        recipient = raw_recipient[2:].strip()
        message = raw_message.strip()
        return recipient, message

    def disconnect_user(self):
        reply = Core().delete_user(self.user_name)
        if reply:
            self.connection.sendall(f"{protocol_flag}exit".encode("utf8"))
            exit()

    def wait_data_from_user(self):
        while True:
            data = self.connection.recv(4096).decode("utf8")
            if data.startswith(r"\exit"):
                self.disconnect_user()
            elif data.startswith(r"\users"):
                Core().get_all_users(self.user_name)
            elif data.startswith(r"->"):
                try:
                    recipient, message = self.prepare_private_message(data)
                except ValueError:
                    continue
                Core().send_message(message, self.user_name, recipient)
            elif data:
                Core().send_message(data, self.user_name)

    def handle(self):
        is_registration_complete = False
        while not is_registration_complete:
            is_registration_complete = self.register_new_user()
            self.connection.sendall(
                f"{protocol_flag}{is_registration_complete}".encode()
            )
        self.wait_data_from_user()


if __name__ == "__main__":
    HOST = "localhost"
    PORT = 60500
    protocol_flag = ":%"
    with ThreadingTCPServer((HOST, PORT), ChatHandler) as chat_server:
        try:
            chat_server.serve_forever()
        except KeyboardInterrupt:
            for conn in Core().current_users_on_server.values():
                conn.sendall(f"{protocol_flag}exit".encode("utf8"))
            chat_server.server_close()
