from concurrent.futures import ThreadPoolExecutor, as_completed
import socket
from threading import Event


def wait_reply_from_server(conn):
    while global_event:
        reply = conn.recv(4096).decode("utf8")
        if reply.startswith(PROTOCOL_FLAG):
            _, protocol_message = reply.split(PROTOCOL_FLAG)
            if protocol_message == "exit":
                global_event.clear()
                exit()
            elif protocol_message == "False":
                print("Nickname has already taken\nTry again: ")
            elif protocol_message:
                print(f"Welcome to Chat, {protocol_message}")
                print(help_info())
        else:
            print(reply)


def help_info():
    border = "-" * 50
    help_message = [
        border,
        r"Available commands:",
        r"\help - this help info",
        r"\exit - for exit",
        r"\users - get current users",
        r"-> <nick_name>: your message - for private message",
        r"After \exit or Ctrl+C press button ENTER",
        border,
    ]
    return "\n".join(help_message)


def wait_input_from_user(conn):
    while global_event:
        message = input()
        if message == r"\help":
            print(help_info())
        else:
            conn.sendall(message.encode("utf8"))


if __name__ == "__main__":
    addr = "localhost"
    port = 60500
    PROTOCOL_FLAG = ":%"
    global_event = Event()
    global_event.set()
    USER_NAME = None
    try:
        with socket.create_connection((addr, port)) as conn:
            print("Enter your nickname: ")
            pool = ThreadPoolExecutor(max_workers=2)
            futures = [
                pool.submit(wait_reply_from_server, conn),
                pool.submit(wait_input_from_user, conn),
            ]
            try:
                for future in as_completed(futures):
                    future.result()

            except KeyboardInterrupt:
                conn.sendall(b"\exit")
                global_event.clear()
                exit()
    except ConnectionRefusedError:
        exit("Chat Server offline")
