ALTER TABLE employee
  DROP CONSTRAINT employee_employee_city_fkey;

ALTER TABLE employee
  ALTER COLUMN employee_city TYPE varchar(255)
  USING employee.employee_city :: varchar(255);

ALTER TABLE department
  DROP CONSTRAINT department_department_city_fkey;

ALTER TABLE department
  ALTER COLUMN department_city TYPE varchar(255)
  USING department.department_city :: varchar(255);

DROP TABLE city;

UPDATE db_scheme_version
SET (db_version, upgraded_on) = ('1.4', now());
