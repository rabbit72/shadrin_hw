ALTER TABLE employee
  DROP CONSTRAINT employee_employee_department_fkey;

ALTER TABLE employee
  ALTER COLUMN employee_department TYPE varchar(255)
  USING employee_department :: varchar(255);

UPDATE db_scheme_version
SET (db_version, upgraded_on) = ('1.5', now());