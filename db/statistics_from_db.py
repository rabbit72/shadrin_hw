import json

import psycopg2
from psycopg2 import sql


def get_statistics_from(user, password=None, host="127.0.0.1", port=5432):
    with psycopg2.connect(user=user, password=password, host=host, port=port) as conn:
        cursor = conn.cursor()
        result = {
            "hw1": fetch_most_popular_name_surname(cursor),
            "hw2": fetch_quantity_employees_different_city(cursor),
            "hw3": fetch_quantity_employees_with_salary_larger_boss(cursor),
            "hw4": fetch_department_with_highest_average_salary(cursor),
            "hw5": fetch_departments_most_different_salary(cursor),
        }
    return json.dumps(result)


def fetch_db_reply(sql_request, cursor):
    cursor.execute(sql.SQL(sql_request))
    return cursor.fetchall()


def fetch_most_popular_name_surname(cursor) -> str:
    sql_request = """
        SELECT first_name, last_name, count(*) as counter 
        FROM employee
        GROUP BY first_name, last_name
        ORDER BY counter DESC
        LIMIT 1;
        """
    reply = fetch_db_reply(sql_request, cursor)
    popular_name = " ".join(reply[0][:2])
    return popular_name


def fetch_quantity_employees_different_city(cursor) -> int:
    sql_request = """
        SELECT count(*)
        FROM employee
        INNER JOIN department ON emploee_department = department_id
        WHERE lower(employee_city) != lower(department_city);
        """
    # """
    # SELECT count(*)
    # FROM employee
    # WHERE lower(employee_city) != lower((
    #   SELECT department_city
    #   FROM department
    #   WHERE lower(department_name) = lower(employee_department)
    #  ));
    # """
    reply = fetch_db_reply(sql_request, cursor)
    quantity_employees = reply[0][0]
    return quantity_employees


def fetch_quantity_employees_with_salary_larger_boss(cursor) -> int:
    sql_request = """
        SELECT count(*)
        FROM employee worker
        WHERE (worker.salary > (SELECT boss.salary
        FROM employee boss
        WHERE boss.employee_id = worker.boss));
        """
    reply = fetch_db_reply(sql_request, cursor)
    quantity_employees = reply[0][0]
    return quantity_employees


def fetch_department_with_highest_average_salary(cursor) -> str:
    sql_request = """
        SELECT department_name FROM department
        INNER JOIN employee ON department_id = emploee_department
        GROUP BY department_name
        ORDER BY SUM(salary) / COUNT(*) DESC
        LIMIT 1;
        """
    reply = fetch_db_reply(sql_request, cursor)
    department_name = reply[0][0]
    return department_name


def fetch_departments_most_different_salary(cursor) -> list:
    sql_request = """
        WITH salaries AS (SELECT emploee_department, salary, NTILE(10) OVER (
        PARTITION BY emploee_department ORDER BY salary asc) AS decile
                  FROM employee)
        SELECT department_id, department_name
        FROM salaries
        INNER JOIN department ON emploee_department = department_id
        GROUP BY department_id, department_name
        ORDER BY 
            SUM(salary) FILTER (WHERE decile = 10) /
            SUM(salary) FILTER (WHERE decile = 1) DESC
        LIMIT 2;
        """
    name_2_departments = fetch_db_reply(sql_request, cursor)
    return name_2_departments


if __name__ == "__main__":
    host = "127.0.0.1"
    port = 5432
    user = "postgres"
    password = "pass"
    print(get_statistics_from(user, password, host, port))
