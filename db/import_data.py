import psycopg2
from psycopg2 import sql


def copy_columns_to_bd(cursor, path, table, columns):
    with open(path) as csv:
        csv.readline()
        cursor.copy_from(csv, table, sep=",", columns=columns, null="")


if __name__ == "__main__":
    host = "127.0.0.1"
    port = 5432
    user = "postgres"
    password = "pass"
    path_to_departments = "./csv/DEPTS.csv"
    path_to_employers = "./csv/EMPLOYEE.csv"

    department_columns = ["department_name", "department_city"]
    emploee_columns = [
        "employee_id",
        "first_name",
        "last_name",
        "employee_department",
        "employee_city",
        "boss",
        "salary",
    ]
    with psycopg2.connect(user=user, password=password, host=host, port=port) as conn:
        cursor = conn.cursor()
        copy_columns_to_bd(
            cursor, path_to_departments, "department", department_columns
        )
        copy_columns_to_bd(cursor, path_to_employers, "employee", emploee_columns)

        change_employee_department = sql.SQL(
            """
ALTER TABLE employee
ADD COLUMN emploee_department_temp INTEGER REFERENCES department (department_id);
    
UPDATE employee
SET (emploee_department_temp) = (SELECT department_id
                                     from department
                                     WHERE lower(department_name) =
                                           lower(employee_department));
                                           
ALTER TABLE employee
  DROP COLUMN employee_department;

ALTER TABLE employee
  RENAME emploee_department_temp TO emploee_department;
            """
        )
        cursor.execute(change_employee_department)
