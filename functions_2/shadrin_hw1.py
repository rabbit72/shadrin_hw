def is_even(n):
    if n == 0:
        return True
    else:
        return is_odd(n - 1)


def is_odd(n):
    if n == 0:
        return False
    else:
        return is_even(n - 1)


def is_digit(text, n=0):
    if n == len(text):
        return True
    if 0 <= ord(text[n]) - ord("0") <= 9:
        return is_digit(text, n+1)
    return False


def get_digit(text, n=0):
    if n == len(text):
        return 0
    number = (ord(text[n]) - ord("0")) * 10 ** (len(text) - (n+1))
    return number + get_digit(text, n+1)


if __name__ == "__main__":
    while True:
        text = input("Enter the text: ")
        if text == "cancel":
            exit("Bye!")
        if is_digit(text):
            digit = get_digit(text)
        else:
            print("Не удалось преобразовать введенный текст в число.")
            continue
        if is_even(digit):
            print(int(digit / 2))
        else:
            print((digit * 3 + 1))
