def count_float_number(accuracy, acc=0):
    if accuracy >= 1:
        return acc
    acc += 1
    accuracy *= 10
    return count_float_number(accuracy, acc)


def mysqrt(number, accuracy=None, start_number=1):
    if abs(start_number**2 - number) < 0.001**2:
        accuracy = number if not accuracy else accuracy
        return round(start_number, ndigits=count_float_number(accuracy))
    start_number = (start_number + number / start_number) / 2
    return mysqrt(number, accuracy, start_number)


if __name__ == "__main__":
    print(mysqrt(4))
    print(mysqrt(5, 0.01))
    print(mysqrt(4/100, 0.1))
    print(mysqrt(4/10_000))
    print(mysqrt(4/1_000_000, 0.0000001))
    print(mysqrt(4/100_000_000))
    print(mysqrt(4e-256))
    print(mysqrt(4e+256))
