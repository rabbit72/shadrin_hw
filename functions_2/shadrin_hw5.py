from time import perf_counter
from functools import lru_cache


def counter(variable_name):
    if variable_name not in globals():
        raise ValueError(f'"{variable_name}" for writing data not found')

    field_time_sum = "all_time"
    field_count = "count"
    globals()[variable_name] = {field_count: 0, field_time_sum: 0}

    def wrapper_1(function):
        def wrapper(*args, **kwargs):
            time_start = perf_counter()
            result = function(*args, **kwargs)
            time_stop = perf_counter()

            globals()[variable_name][field_count] += 1
            globals()[variable_name][field_time_sum] += time_stop - time_start

            return result

        return wrapper

    return wrapper_1


def pow(x, n, I, mult):
    if n == 0:
        return I
    elif n == 1:
        return x
    else:
        y = pow(x, n // 2, I, mult)
        y = mult(y, y)
        if n % 2:
            y = mult(x, y)
        return y


def identity_matrix(n):
    r = list(range(n))
    return [[1 if i == j else 0 for i in r] for j in r]


def matrix_multiply(A, B):
    BT = list(zip(*B))
    return [[sum(a * b
                 for a, b in zip(row_a, col_b))
             for col_b in BT]
            for row_a in A]


keeper1 = None
keeper2 = None
keeper3 = None
keeper4 = None
keeper5 = None


@counter("keeper1")
def fib1(n):
    sqrt5 = 5 ** 0.5
    pi = (sqrt5 + 1) / 2
    return int(pi ** n / sqrt5 + 0.5)


@lru_cache()
@counter("keeper3")
def fib3(n):
    if n < 2:
        return n
    return fib3(n - 1) + fib3(n - 2)


@counter("keeper4")
def fib4(n):
    a = 0
    b = 1
    for __ in range(n):
        a, b = b, a + b
    return a

# the fastest method
@counter("keeper5")
def fib5(n):
    f = pow([[1, 1], [1, 0]], n, identity_matrix(2), matrix_multiply)
    return f[0][1]


if __name__ == "__main__":
    n = 10**6
    keepers = (keeper1, keeper3, keeper4, keeper5)
    print(fib4(n))
    print(fib5(n))
    print("\n".join(map(str, keepers)))
