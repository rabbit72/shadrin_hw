def has_triangle_point(point, a, b, c):
    sum_square = get_square(point, a, b) + get_square(point, b, c) + get_square(point, c, a)
    return abs(get_square(a, b, c) - sum_square) == 0


def get_square(a, b, c):
    a_x, a_y = a
    b_x, b_y = b
    c_x, c_y = c
    return abs(b_x*c_y - c_x*b_y - a_x*c_y + c_x*a_y + a_x*b_y - b_x*a_y)


def count_points(a, b, c):
    counter = 0
    triangle = (a, b, c)
    min_x = min([x[0] for x in triangle])
    min_y = min([x[1] for x in triangle])
    max_x = max([x[0] for x in triangle])
    max_y = max([x[1] for x in triangle])
    points = [(x, y) for x in range(min_x, max_x + 1) for y in range(min_y, max_y + 1)]
    for point in points:
        counter += has_triangle_point(point, a, b, c)
    return counter


if __name__ == "__main__":
    print(count_points((0, 0), (1, 0), (0, 1)))
    print(count_points((0, 0), (2, 0), (0, 2)))
    print(count_points((-2, -2), (0, 4), (4, 3)))
