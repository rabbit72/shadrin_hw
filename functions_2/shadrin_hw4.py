from datetime import datetime, timedelta
import time


def make_cache(life_time):
    life_time = timedelta(seconds=life_time)

    def create_cache(function):
        cache = {}

        def wrapper(*args):
            if args in cache and datetime.now() - cache[args][1] < life_time:
                return cache[args][0]
            else:
                val = function(*args)
                cache[args] = val, datetime.now()
                return val
        return wrapper
    return create_cache


@make_cache(3)
def fib(n):
    if n < 2:
        return n
    return fib(n-1) + fib(n-2)


if __name__ == "__main__":
    i = 332
    print("result", fib(i))
    time.sleep(1)
    i = 600
    print("result", fib(i))
    time.sleep(1)
    i = 900
    print("result", fib(i))
    time.sleep(2)
    i = 333
    print("result", fib(i))
