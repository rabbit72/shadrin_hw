from contracts import contract, disable_all

disable_all()

"""
timeit get_min_positive_number(N)
repeats - 100_000
average execution time (seconds) for:
    len(N) = 10 - 0.10691683800541796
    len(N) = 100 - 0.5731394430040382
    len(N) = 1000 - 6.736299462005263
    len(N) = 10000 - 78.21557153199683
    
This function is linear
"""


@contract
def get_min_positive_number(numbers):
    """
    :type numbers: list
    :rtype: int, > 0
    """
    min_value = 1
    unique_numbers = set(numbers)
    for _ in range(len(unique_numbers) + 1):
        if min_value not in unique_numbers:
            return min_value
        min_value += 1


@contract
def get_numbers(string):
    """
    :type string: str
    :rtype: list[N], N > 0
    """
    return list(map(int, string.split()))


if __name__ == "__main__":
    string_numbers = input()
    min_positive = get_min_positive_number(get_numbers(string_numbers))
    print(min_positive)
