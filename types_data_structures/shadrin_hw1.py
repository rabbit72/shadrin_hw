def remove_duplicates(words):
    unique_words = []
    for word in words:
        if word in unique_words:
            continue
        unique_words.append(word)
    return unique_words


if __name__ == "__main__":
    words = input().split(" ")
    words_without_duplicates = remove_duplicates(words)
    print(" ".join(words_without_duplicates))
