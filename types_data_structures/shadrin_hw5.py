def is_palindrome(number):
    return str(number) == str(number)[::-1]


def get_sum_all_palindromes(end, start=1):
    """
    Return a sum of all palindromes in base 10 and base 2
    """
    sum_all_palindromes = 0
    for number in range(start, end, 2):
        if is_palindrome(number) and is_palindrome(format(number, "b")):
            sum_all_palindromes += number
    return sum_all_palindromes


if __name__ == "__main__":
    end = 1000000
    print(get_sum_all_palindromes(end))
