import re


def get_numbers(string):
    str_numbers = re.findall(r"[+-]?\d+", string)
    numbers = map(int, str_numbers)
    return list(numbers)


if __name__ == "__main__":
    raw_string = input()
    numbers_sum = sum(get_numbers(raw_string))
    print(numbers_sum)
