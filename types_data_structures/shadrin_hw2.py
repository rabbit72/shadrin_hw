import re
from collections import Counter


def get_words_from_text(text):
    words = re.findall(r"\w+", text.lower())
    return words


def get_most_frequent_word(words):
    most_frequent = []
    all_words = Counter(words).most_common()
    max_quantity = all_words[0][1]
    for word, quantity in all_words:
        if max_quantity != quantity:
            break
        most_frequent.append((word, quantity))
    return most_frequent


def print_most_frequent(words):
    for word, quantity in words:
        print("{} - {}".format(quantity, word))


if __name__ == "__main__":
    while True:
        text = input()
        if text == "cancel":
            exit("Bye!")
        words = get_words_from_text(text)
        print_most_frequent(get_most_frequent_word(words))
