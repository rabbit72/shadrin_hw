import unittest

from .shadrin_hw3 import collatz_steps


class TestCollatzSteps(unittest.TestCase):
    def test_collatz_steps(self):
        cases = [
            (16, 4),
            (12, 9),
            (1_000_000, 152),
        ]
        for args, result in cases:
            with self.subTest(args=args):
                self.assertEqual(collatz_steps(args), result)