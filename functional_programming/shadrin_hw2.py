from functools import reduce


def is_armstrong(number):
    return number == reduce(
        lambda x, y: x + y,
        map(
            lambda x: x ** len(list(map(int, str(number)))),
            map(int, str(number))
        )
    )
