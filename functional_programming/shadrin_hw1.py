from functools import reduce
from math import sqrt


def get_full_squares(limit):
    x = 0
    while True:
        square = x**2
        if square >= limit:
            raise StopIteration
        yield square
        x += 1


if __name__ == "__main__":
    n = 10**6

    # the first
    squares1 = [x for x in range(n) if (x ** 0.5).is_integer()]
    s1 = sum(squares1)

    # the second
    squares2 = []
    for x in range(n):
        if sqrt(x) % 1 == 0:
            squares2.append(x)
    s2 = reduce(lambda x, y: x+y, squares2)
    
    # the third
    s3 = 0
    squares3 = list(get_full_squares(n))
    for x in squares3:
        s3 += x
    print(s1, s2, s3)

