def collatz_steps(n, i=0):
    if n == 1:
        return i
    n = n * 3 + 1 if n % 2 else n / 2
    return collatz_steps(n, i + 1)
