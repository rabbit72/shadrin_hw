from operator import sub, mul
from functools import reduce


if __name__ == "__main__":

    # problem 9
    print([(a, b, c) for a in range(1, 1000) for b in range(a + 1, 1000 - a) for c in (1000 - a - b,) if a ** 2 + b ** 2 == c ** 2][-1])

    # problem 6
    print(sub(sum([x for x in range(101)]) ** 2, sum([x**2 for x in range(101)])))

    # problem 48
    print(str(sum([x**x for x in range(1, 1001)]))[-10:])

    # problem 40
    print(reduce(mul, map(int, ["".join([str(x) for x in range(1, 1000001)])[x-1] for x in [10**i for i in range(7)]])))
