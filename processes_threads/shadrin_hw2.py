import threading
import time

sem = threading.Semaphore()


def fun1():
    while event.wait():
        with sem:
            print(1)
        time.sleep(0.25)


def fun2():
    while event.wait():
        with sem:
            print(2)
        time.sleep(0.25)


event = threading.Event()
event.set()

t1 = threading.Thread(target=fun1, daemon=False)
t1.start()
t2 = threading.Thread(target=fun2, daemon=False)
t2.start()
try:
    t1.join()
    t2.join()
except KeyboardInterrupt:
    event.clear()

