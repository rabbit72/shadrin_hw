import asyncio
import os
import socket
import time
from concurrent.futures import ThreadPoolExecutor, as_completed


start_number = 0
stop_number = 5

server_address = "127.0.0.1"
# how many server do you want to test?
ports = [8000, 8001, 8002, 8003]

pprint = True
servers = [(server_address, port) for port in ports]

max_workers = os.cpu_count()

config = [servers, start_number, stop_number, max_workers, pprint]


async def fetch_request_async(message: str, loop, addr, port):
    reader, writer = await asyncio.open_connection(addr, port, loop=loop)
    writer.write(message.encode("utf8"))
    result = await reader.read(1024)
    writer.close()
    # print(message)
    # return result.decode()
    return message


def test_with_asyncio(config):
    times = {}
    loop = asyncio.get_event_loop()
    servers, start, stop, _, pprint = config
    for addr, port in servers:
        coroutines = [
            fetch_request_async(str(item), loop, addr, port)
            for item in range(start, stop)
        ]
        future = asyncio.gather(*coroutines)
        t1 = time.time()
        """return list with safe sequence results"""
        results = loop.run_until_complete(future)

        """return set with unsafe sequence results"""
        # results = loop.run_until_complete(asyncio.wait(coroutines))

        t2 = time.time()
        work_time = t2 - t1
        if pprint:
            print(f"{addr}: {port} - ", work_time)
        times[(addr, port)] = work_time
    loop.close()
    return times


def test_with_thread_pool(config):
    times = {}
    servers, start, stop, max_workers, pprint = config

    for addr, port in servers:
        with ThreadPoolExecutor(max_workers=max_workers) as pool:
            futures = [
                pool.submit(fetch_request, str(item), addr, port)
                for item in range(start, stop)
            ]
            t1 = time.time()
            for future in as_completed(futures):
                future.result()
            # print(work[0].result())
            t2 = time.time()
        work_time = t2 - t1
        if pprint:
            print(f"{addr}: {port} - ", work_time)
        times[(addr, port)] = work_time
    return times


def fetch_request(item, addr="127.0.0.1", port=8000):
    with socket.create_connection((addr, port)) as sock:
        sock.sendall(item.encode("utf8"))
        result = sock.recv(1024).decode("utf8")
    return result


r1 = test_with_asyncio(config)
print("-" * 40)
r2 = test_with_thread_pool(config)
