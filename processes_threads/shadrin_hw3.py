import socket
import multiprocessing
import math
import hashlib
import os
import sys
import signal

"""
all measurements of speed were got during processing 6 thousand
requests from 0 to 6000

8000 - 1 worker
8001 - 2 worker
8002 - 4 worker
8003 - 8 worker

Time is in seconds 
127.0.0.1: 8000 -  15.653272867202759
127.0.0.1: 8001 -  7.158605098724365
127.0.0.1: 8002 -  3.530259370803833
127.0.0.1: 8003 -  2.923180103302002
"""


def main_server_function(port: int = 8000, num_of_workers: int = 2):
    """
    :param port : port number to accept the incoming requests
    :param num_of_workers : number of workers to handle the requests
    """
    if port > 65535:
        raise AttributeError("Port can't be < 65535")

    with socket.socket() as sock:
        sock.bind(("", port))
        sock.listen(5)
        children = []
        for i in range(num_of_workers):
            pid = os.fork()
            if pid:
                children.append(pid)
            if pid == 0:
                childpid = os.getpid()
                print(f"Child {childpid} listening on localhost:{port}")
                while True:
                        conn, addr = sock.accept()
                        processing_new_connection(conn, addr)
        try:
            os.waitpid(-1, 0)
        except KeyboardInterrupt:
            for pid in children:
                os.kill(pid, signal.SIGKILL)
            print("\nexit")
            sys.exit()


def processing_new_connection(conn, addr):
    with conn:
        while True:
            data = conn.recv(1024)
            if not data:
                break
            try:
                message = data.decode("utf8")
            except UnicodeDecodeError:
                conn.sendall(f"bad request {data}\n".encode("utf8"))
                break
            result = processing_request(message)
            conn.sendall(f"{result}\n".encode("utf8"))


def processing_request(request: str) -> str:
    try:
        number = int(request)
    except ValueError:
        return "it's not integer"

    factorial = math.factorial(number)
    hash_obj = hashlib.md5(str(factorial).encode("utf8"))
    md5 = hash_obj.digest()
    return str(md5)


if __name__ == "__main__":
    port = sys.argv[1]
    workers = sys.argv[2]
    main_server_function(int(port), int(workers))
