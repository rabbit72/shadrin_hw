import getpass
import psutil


def process_count(username: str = None) -> int:
    # к-во процессов, запущенных из-под текущего пользователя
    if not username:
        username = getpass.getuser()
    return len([p for p in psutil.process_iter() if p.username() == username])


def total_memory_usage(root_pid: int = None) -> int:
    # суммарное потребление памяти древа процессов
    if not root_pid:
        root_pid = 1
    root_process = psutil.Process(root_pid)
    children_memory = root_process.memory_info().rss
    for child in root_process.children(recursive=True):
        children_memory += child.memory_info().rss
    return children_memory


if __name__ == "__main__":
    print(process_count())
    print(total_memory_usage())
