class EnumMeta(type):
    def __new__(mcs, name_class, bases, dct):
        user_attr = mcs.__get_user_attr_from_enum(dct)
        name_instance = mcs.__create_instances(name_class, user_attr)
        value_instance = {value.value: value for key, value in name_instance.items()}
        for_update = name_instance.copy()
        for_update["members__"] = name_instance
        for_update["values_members__"] = value_instance

        for_update["members_list__"] = list(name_instance.values())
        # for_update["members_list__"] = [f"{name_class}.{attr}"for attr in user_attr.keys()]

        dct.update(for_update)
        return super().__new__(mcs, name_class, bases, dct)

    def __call__(cls, value, *args, **kwargs):
        value_members = cls.__dict__["values_members__"]
        if value in value_members:
            return value_members[value]
        raise ValueError(f"{value} is not a valid {cls.__name__}")

    def __iter__(cls):
        return iter(cls.__dict__["members_list__"])

    def __getitem__(cls, item):
        enum_instances = cls.__dict__["members__"]
        if item in enum_instances:
            return enum_instances[item]
        raise KeyError(item)

    @staticmethod
    def __get_user_attr_from_enum(dct):
        user_attr = {}
        for name, val in dct.items():
            if str(name)[-2:] == "__":
                continue
            user_attr[name] = val
        return user_attr

    @staticmethod
    def __create_instances(name_class, user_attributes: dict):
        enum_instances = {}
        unique_values = set()
        for name, value in user_attributes.items():
            if value in unique_values:
                raise ValueError("All attributes must be different")
            unique_values.add(value)
            instance = EnumMember(name, value, name_class)

            enum_instances[name] = instance
        return enum_instances


class EnumMember:
    def __init__(self, name, value, name_class):
        self.name = name
        self.value = value
        self.__name_class = name_class

    def __repr__(self):
        return f"<{self.__name_class}.{self.name}: {self.value}>"


class Enum(metaclass=EnumMeta):
    r = 1
    d = 2


class Direction(Enum):
    north = 0
    east = 90
    south = 180
    west = 270


print(Direction.north)  # <Direction.north: 0>
print(Direction.south)  # <Direction.south: 180>
print(Direction.north.name)  # north
print(Direction.north.value)  # 0

for d in Direction:
    print(d)

print(id(Direction.north), Direction.north is Direction(0))

# Direction(30)  # ValueError: 30 is not a valid Direction

print(Direction['west'])  # <Direction.west: 270=0>
# print(Direction['north-west'])  # KeyError: 'north-west'
