class OurDescriptor:
    def __init__(self, name=None):
        self.descriptor = None
        self.name = name

    def __get__(self, instance, owner):
        return f"{self.descriptor}_{self.name}"

    def __set__(self, instance, value):
        if value < 0:
            raise ValueError
        self.descriptor = value


some_value = OurDescriptor()


class BePositive:
    some_value = OurDescriptor('some')
    another_value = OurDescriptor('another')

    def something(self):
        self.in_method = OurDescriptor()


instance = BePositive()
# должно работать
instance.some_value = 1
print(instance.some_value)
# должно выкинуть ошибку
try:
    instance.another_value = -5
except ValueError:
    print("Error")
print(instance.another_value)

instance.in_method = -64646
print(instance.in_method)

some_value = -5
print(some_value)
