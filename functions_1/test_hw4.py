import unittest

from .shadrin_hw4 import letters_range


class TestLettersRange(unittest.TestCase):
    def test_correct_arguments(self):
        cases = [
            ("a", []),
            ("h", ["a", "b", "c", "d", "e", "f", "g"]),
            (("d", "l"), ["d", "e", "f", "g", "h", "i", "j", "k"]),
            (("z", "u", -1), ["z", "y", "x", "w", "v"]),
            (("z", "h", -5), ["z", "u", "p", "k"]),
        ]
        for args, result in cases:
            with self.subTest(args=args):
                self.assertEqual(letters_range(*args), result)

    def test_empty_arguments(self):
        self.assertRaises(TypeError, letters_range)

    def test_wrong_argument_with_exception(self):
        cases = ["wrong", "Q", ("Q", "R"), ("R", "f", 5), -5, (-2, 2, 2), (1, 4)]
        for args in cases:
            with self.subTest(args=args):
                self.assertRaises(ValueError, letters_range, args)

    def test_wrong_argument_without_exception(self):
        cases = [("l", "d"), ("z", "h", 1), ("z", "h", 5)]
        for args in cases:
            with self.subTest(args=args):
                self.assertEqual(letters_range(*args), [])


if __name__ == "__main__":
    unittest.main()
