def letters_range(start_letter=None, stop_letter=None, step=1):
    if not(start_letter or stop_letter):
        raise TypeError("letters_range expected 1 arguments, got 0")

    alphabet = "abcdefghijklmnopqrstuvwxyz"
    alpha_dict = {letter: position for position, letter in enumerate(alphabet)}

    if start_letter and not stop_letter:
        stop_letter = start_letter
        start_letter = "a"
    try:
        start = alpha_dict[start_letter]
        stop = alpha_dict[stop_letter]
    except KeyError:
        raise ValueError("unsupported operand value(s)")

    return list(alphabet[start:stop:step])


if __name__ == "__main__":
    pass
