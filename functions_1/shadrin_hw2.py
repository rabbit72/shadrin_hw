def make_it_count(func, counter_name):
    def wrapper(*args, **kwargs):
        globals()[counter_name] += 1
        return func(*args, **kwargs)

    return wrapper


if __name__ == "__main__":
    counter = 0
    incr_print = make_it_count(print, 'counter')
    print(counter, incr_print(), counter)
