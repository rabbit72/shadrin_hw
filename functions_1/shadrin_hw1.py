from inspect import signature


def get_string_fix_arguments(func, args, kwargs):
    try:
        bound_obj = signature(func).bind_partial(*args, **kwargs)
        string_args = str(bound_obj)[16:-1]
    except ValueError:
        list_kwargs = []
        for key, value in kwargs.items():
            list_kwargs.append(f"{key}={value}")
        string_args = (*args, *list_kwargs)
    finally:
        return f"{func.__name__}{string_args}"


def partial(func, *fixated_args, **fixated_kwargs):
    def wrapper(*args, **kwargs):
        new_args = fixated_args + args
        new_kwargs = {**fixated_kwargs, **kwargs}
        return func(*new_args, **new_kwargs)

    string_fix_arguments = get_string_fix_arguments(
        func,
        fixated_args,
        fixated_kwargs
    )
    wrapper.__name__ = f"partial_{func.__name__}"
    wrapper.__doc__ = f"""
A partial implementation of {func.__name__}
with pre-applied arguments being:
{string_fix_arguments}
"""
    return wrapper


if __name__ == "__main__":

    def mult4(x, y, z=1, xx=1):
        return x * y * z * xx

    print(mult4(2, 3, xx=4))
    test1 = partial(mult4, 2, 3, xx=4)
    print(test1.__name__)
    print(test1.__doc__)
    print(test1())
