def atom(box=None):
    def get_value():
        return box

    def set_value(value):
        nonlocal box
        box = value
        return box

    def process_value(*functions):
        nonlocal box
        for func in functions:
            box = func(box)
        return box

    return get_value, set_value, process_value


if __name__ == "__main__":
    get, set, process = atom()
    print(get())
    print(set(5))
    process(lambda x: x + 1, lambda x: x - 2)
    print(get())
