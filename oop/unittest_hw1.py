import unittest
from oop.shadrin_hw1 import Calculator


class TestCalculator(unittest.TestCase):
    def test_validate(self):
        cases = [
            ("a+2", True),
            ("a-(-2)", True),
            ("a+2-", False),
            ("a+(2+(3+5)", False),
            ("a^2", True),
            ("a^(-2)", True),
            ("-a-2", True),
            ("6/0", False),
            ("a/(b-b)", True),
        ]
        for args, result in cases:
            with self.subTest(args=args):
                calc = Calculator(list(args))
                self.assertEqual(calc.validate(), result)

    def test_str(self):
        cases = [
            ("a", "a"),
            ("-a", "a -"),
            ("(a*(b/c)+((d-f)/k))", "a b c / * d f - k / +"),
            ("(a)", "a"),
            ("a*(b+c)", "a b c + *"),
            ("(a*(b/c)+((d-f)/k))*(h*(g-r))", "a b c / * d f - k / + h g r - * *"),
            ("(x*y)/(j*z)+g", "x y * j z * / g +"),
            ("a-(b+c)", "a b c + -"),
            ("a/(b+c)", "a b c + /"),
            ("a^(b+c)", "a b c + ^"),
            ("x^y/(r*z)+g^z", "x y ^ r z * / g z ^ +"),
        ]
        for args, result in cases:
            with self.subTest(args=args):
                calc = Calculator(list(args))
                self.assertEqual(str(calc), result)
