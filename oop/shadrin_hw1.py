import re


class Calculator:
    def __init__(self, opcodes: list, operators: list = None):
        self.opcodes = opcodes
        self.operators = operators if operators is not None else []

    def __str__(self) -> str:
        if not self.is_brackets_correct():
            raise ValueError("{0} non-valid expression".format(self.opcodes))
        return " ".join(self.get_postfix())

    def optimise(self):
        for operator in self.operators:
            self.opcodes = operator.process(self.opcodes)

    def is_brackets_correct(self):
        stack = []
        for symbol in self.opcodes:
            if symbol == "(":
                stack.append(symbol)
            elif symbol == ")":
                try:
                    stack.pop()
                except IndexError:
                    return False
        brackets = False if stack else True
        return brackets

    @staticmethod
    def available_operations():
        return {"+": 1, "-": 1, "*": 2, "/": 2, "^": 3, None: 4}

    def validate(self) -> bool:
        return (
            self.is_brackets_correct()
            and self.has_zero_division()
            and self.is_possible_calculate()
        )

    def has_zero_division(self):
        string = "".join(self.get_postfix())
        if "0/" in string:
            return False
        return True

    def is_possible_calculate(self):
        string = "".join(self.opcodes)
        operator_before_bracket = r"[^(]?[-+/*^][)]"
        operator_after_bracket = r"[-+/*^]$"
        side_by_side_operators = r"[-+/*^]{2,}"
        pattern = r"{}|{}|{}".format(
            operator_before_bracket,
            operator_after_bracket,
            side_by_side_operators,
        )
        wrong_operators = re.findall(pattern, string)
        if wrong_operators:
            return False
        return True

    def get_postfix(self) -> list:
        operations = self.available_operations()
        stack = []
        output = []
        for symbol in self.opcodes:
            if not symbol:
                continue
            elif symbol.isalnum():
                output.append(symbol)
            elif symbol == "(":
                stack.append(symbol)
            elif symbol in ")":
                while stack and stack[-1] != "(":
                    output.append(stack.pop())
                stack.pop()
            elif symbol in operations:
                while stack and operations.get(stack[-1], 0) >= operations[symbol]:
                    output.append(stack.pop())
                stack.append(symbol)
        for operation in stack[::-1]:
            output.append(operation)
        return output
