import unittest
from oop.shadrin_hw1 import Calculator
from oop.shadrin_hw2 import (
    DoubleNegativeOptimiser,
    IntegerCostantsOptimiser,
    UnnecessaryOperationsOptimiser,
)


class TestOptimisers(unittest.TestCase):
    def test_double_negate(self):
        cases = [
            ("-(-a)", "a"),
            ("-(-5)", "5"),
            ("-(a+b)+c-(-d)", "a b + - c + d +"),
        ]
        for args, result in cases:
            with self.subTest(args=args):
                tokens = list(args)
                calc = Calculator(tokens, [DoubleNegativeOptimiser()])
                calc.optimise()
                self.assertEqual(str(calc), result)

    def test_integer_constant_optimiser(self):
        cases = [
            (["1"], ["1"]),
            (["1", "+", "2"], ["3"]),
            (["1", "-", "2"], ["1 -"]),
            (["2", "*", "2"], ["4"]),
            (["2", "/", "2"], ["1"]),
            (["2", "^", "10"], ["1024"]),
            (["a", "+", "2", "*", "4"], ["a 8 +", "8 a +"]),
            # (["2", "+", "a", "+", "3"], ["5 a +", "a 5 +"]),  # (*)
        ]
        for args, result in cases:
            with self.subTest(args=args):
                calc = Calculator(
                    args, [DoubleNegativeOptimiser(), IntegerCostantsOptimiser()]
                )
                calc.optimise()
                self.assertIn(str(calc), result)

    def test_simplifier_optimiser(self):
        cases = [
            ("a+0", ["a"]),
            ("a*1", ["a"]),
            ("a*0", ["0"]),
            ("b/b", ["1"]),
            ("a-a", ["0"]),
            ("a+(b-b)", ["a"]),
            ("a+(7-6-1)", ["a"]),
            ("a^0", ["1"]),
            ("a-(-(-a))", ["0"]),
            ("(-a)^2", "a^2"),
            ("-(-a)^3", "a^3"),
            # ("a+a+a", ["a 3 *", "3 a *"]),  # (*)
            # ("(a-b)-(a-b)", ["0"]),  # (*)
            # ("(a-b)/(a-b)", ["1"]),  # (*)
            # ("(a+b)+(a+b)", ["a b + 2 *", "2 a b + *"]),  # (*)
            # ("a*b+a*b", ["2 a b * *", "2 b a * *", "a 2 b * *", "a b 2 * *", "b 2 a * *", "b a 2 * *"]),  # (*)
        ]
        for args, result in cases:
            with self.subTest(args=args):
                tokens = list(args)
                calc = Calculator(
                    tokens,
                    [
                        DoubleNegativeOptimiser(),
                        IntegerCostantsOptimiser(),
                        UnnecessaryOperationsOptimiser(),
                    ],
                )
                calc.optimise()
                self.assertIn(str(calc), result)
