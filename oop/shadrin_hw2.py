import operator


class AbstractOptimiser:
    def process(self, graph):
        g = self.pre_process(graph)

        result = self.process_internal(g)

        return self.post_process(result)

    def pre_process(self, graph):
        return self._get_postfix(graph)

    def process_internal(self, graph):
        return graph

    def post_process(self, result):
        return self._get_infix(result)

    @staticmethod
    def _is_digit(s: str) -> float:
        try:
            float(s)
            return True
        except ValueError:
            return False

    def _get_postfix(self, infix: list) -> list:
        operations = self.available_operations()
        stack = []
        output = []
        for symbol in infix:
            if not symbol:
                continue
            elif symbol.isalpha() or self._is_digit(symbol):
                output.append(symbol)
            elif symbol == "(":
                stack.append(symbol)
            elif symbol in ")":
                while stack and stack[-1] != "(":
                    output.append(stack.pop())
                stack.pop()
            elif symbol in operations:
                while stack and operations.get(stack[-1], 0) >= operations[symbol]:
                    output.append(stack.pop())
                stack.append(symbol)
        for operation in stack[::-1]:
            output.append(operation)
        return output

    def _get_infix(self, postfix):
        operations = self.available_operations()
        stack = []
        for symbol in postfix:
            if symbol.isalpha() or self._is_digit(symbol):
                stack.append([symbol, None])
            elif symbol == "-" and len(stack) == 1:
                element, i = stack.pop()
                stack.append([f"-({element})", i])
            elif symbol == "+" and len(stack) == 1:
                element, i = stack.pop()
                stack.append([f"{element}", i])
            elif symbol in operations:
                second = stack.pop()
                first = stack.pop()
                priority_current = operations[symbol]
                priority_first = operations.get(first[1])
                priority_second = operations.get(second[1])

                unusual_case = priority_second == priority_current and symbol in {
                    "/",
                    "-",
                }
                if priority_second < priority_current or unusual_case:
                    second[0] = "(" + second[0] + ")"
                if priority_first < priority_current:
                    first[0] = "(" + first[0] + ")"
                expression = first[0] + symbol + second[0]
                stack.append([expression, symbol])
        return stack.pop()[0]

    @staticmethod
    def available_operations():
        return {"+": 1, "-": 1, "*": 2, "/": 2, "^": 3, None: 4}
    
    @staticmethod
    def operations():
        return {
            "+": operator.add,
            "-": operator.sub,
            "*": operator.mul,
            "/": operator.floordiv,
            "^": operator.pow,
        }


class DoubleNegativeOptimiser(AbstractOptimiser):
    def process_internal(self, postfix):
        stack = []
        last_operator = None
        last_first = None
        last_second = None
        for symbol in postfix:
            if symbol.isalpha() or self._is_digit(symbol):
                stack.append(symbol)
            elif symbol == "-" and not last_operator:
                if stack[-1] == "-":
                    stack.pop()
                    symbol = "+"
                stack.append(symbol)
            elif symbol == "-" and len(stack) == 1 and last_operator == "-":
                stack.pop()
                stack.append([last_first, last_second, "+"])
            elif symbol == "-" and len(stack) == 1 and last_operator == "+":
                stack.pop()
                stack.append([last_first, last_second, last_operator, "-"])
            elif symbol in self.available_operations():
                last_operator = symbol
                second, first = stack.pop(), stack.pop()
                last_first, last_second = first, second
                stack.append([first, second, symbol])
        return self.__unpack_list(stack)

    def __unpack_list(self, x):
        if isinstance(x, list):
            return [a for i in x for a in self.__unpack_list(i)]
        else:
            return [x]


class IntegerCostantsOptimiser(AbstractOptimiser):
    def process_internal(self, postfix):
        stack = []
        while postfix:
            a = postfix.pop(0)
            if a in self.operations() and len(stack) >= 2:
                if a == "+" and stack[-2] == "+" and stack[-1].isdigit():
                    for i in range(len(stack)):
                        if stack[i].isdigit():
                            stack[i] = str(
                                self.operations()[a](int(stack[i]), int(stack.pop()))
                            )
                            break
                elif a == "*" and stack[-2] == "*" and stack[-1].isdigit():
                    for i in range(len(stack)):
                        if stack[i].isdigit():
                            stack[i] = str(
                                self.operations()[a](int(stack[i]), int(stack.pop()))
                            )
                            break
                elif stack[-1].isdigit() and stack[-2].isdigit():
                    res = self.operations()[a](int(stack.pop(-2)), int(stack.pop()))
                    if res < 0:
                        res = [str(res)[1:], "-"]
                        stack.extend(res)
                    stack.append(str(res))
                    continue
            stack.append(a)
        return stack


class UnnecessaryOperationsOptimiser(AbstractOptimiser):
    def process_internal(self, postfix):
        stack = []
        while postfix:
            a = postfix.pop(0)
            if a in self.operations() and len(stack) >= 2:
                if a == "+":
                    if stack[-1] == "0":
                        stack.pop()
                    elif stack[-2] == "0":
                        stack.pop(-2)
                elif a == "*":
                    if stack[-1] == "1":
                        stack.pop()
                    elif stack[-2] == "1":
                        stack.pop(-2)
                    elif stack[-1] == "0":
                        stack.pop()
                        stack[-1] = "0"
                    elif stack[-2] == "0":
                        stack.pop(-2)
                        stack[-1] = "0"
                elif a == "-":
                    if stack[-1] == stack[-2]:
                        stack.pop()
                        stack[-1] = "0"
                    elif stack[-1] == "0":
                        stack.pop()
                    elif stack[-2] == "0":
                        stack.pop(-2)
                elif a == "/" and stack[-1] == stack[-2]:
                    stack.pop()
                    stack[-1] = "1"
                elif a == "^":
                    if stack[-2] == "0":
                        stack.pop(-2)
                        stack[-1] = "1"
                    elif stack[-1] == "0":
                        stack.pop()
                        stack[-1] = "1"
                continue
            stack.append(a)
        if (
            stack[: (len(stack) - 1) // 2] == stack[(len(stack) - 1) // 2 : -1]
            and len(stack) > 3
        ):
            if stack[(len(stack) - 1) // 2 - 1] in self.operations():
                if stack[-1] == "-":
                    stack.clear()
                    stack.append("0")
                if stack[-1] == "/":
                    stack.clear()
                    stack.append("1")
        return stack
